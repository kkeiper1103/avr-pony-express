#include <stdlib.h>
#define MAX_SPEED 0
#define Kp 1
#define Ki 1
#define Kd 1

#include <pololu/orangutan.h>

int last_proportional = 0;
int integral = 0;



void calcTurn(int readLineRes){
	
	int position = readLineRes;
	int prop = position;

	integral = integral + prop;

	int derivative = prop - last_proportional;

	last_proportional = prop;

	//int error = (int)(readLineRes / 16);

	int error = (int)(prop * Kp + integral * Ki + derivative * Kd);

/*	if(error < -256){
		error = -256;
	}
	if(error > 256){
		error = 256;
	}*/

	printf("Error: %d", error);
	 
	int motorSpeed = (int)((MAX_SPEED + error) / 3 );
/*
	if(error < -127){
		set_motors( motorSpeed, motorSpeed );
	} else if(error > 127){
		set_motors( motorSpeed, motorSpeed );
	} else {
		set_motors( 127,  -127);
	}
*/

	

}


int main(){
	
	lcd_init_printf();

	//printf("Trying Something New...");

	unsigned char pins[8] = {0,1,2,3,4,5};
	unsigned char numSensors = 8;
	unsigned char numSamples = 4;

	unsigned int sensorValues[8];

	qtr_analog_init(pins, numSensors, numSamples, 255);
	
	qtr_emitters_on();

	for(int z = 0; z<100;z++){
		clear();
		printf("Calibrating...");
		qtr_calibrate(QTR_EMITTERS_ON);
		delay(50);
	}
	clear();

	while( 1 ){
		calcTurn((qtr_read_line(sensorValues, QTR_EMITTERS_ON) - 3500));
		delay(50);
		clear();
	}

	return 0;
}
